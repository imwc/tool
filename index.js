#!/usr/bin/env node
require('colors');
const http = require('http');
const shell = require('shelljs');
const iconv = require('iconv-lite');
const { program } = require('commander');

let pkg = require('./package.json');

program
  .version(pkg.version.green)
  .name("cctool")
  .usage("[options] [command]");

// 升级
program
  .command('upgrade')
  .description('Fetch the newest version of this tool.')
  .action(() => {
    console.log(`current version: ${pkg.version.green}`);
    console.log('☕️ 🍞 about upgrade ... 🐌 ');

    if (0 === shell.exec('npm i -g git+https://gitlab.com/imwc/tool.git').code) {
      delete require.cache[require.resolve('./package.json')];
      pkg = require('./package.json');
      console.log(`latest version: ${pkg.version.green}`);
      console.log('Upgrade OK'.green);
      process.exit();
    }
    console.log('Upgrade Failed'.red);
    process.exit();

  });

// 金融
program
  .command('finance')
  .description('Finance info.')
  .action(() => {
    const pad = (str, length) => {
      const strlen = str.length;
      return `${' '.repeat(Math.floor((length - strlen) / 2))}${str}${' '.repeat(Math.ceil((length - strlen) / 2))}`;
    }
    const format = (content) => {
      const reg = /".*"/ig;
      content = content.match(reg);
      content = content.map(item => item.replace(/"/ig, ''));
      console.log('┌──────────────┬──────────────┬──────────┬──────────────┬──────────────┬──────────────┬──────────────┬──────────┐');
      console.log(`│${pad('名称', 12)}│${pad('最新', 12)}│${pad('涨跌幅', 7)}│${pad('昨收', 12)}│${pad('今开', 12)}│${pad('最高', 12)}│${pad('最低', 12)}│${pad('振幅', 8)}│`);
      console.log('├──────────────┼──────────────┼──────────┼──────────────┼──────────────┼──────────────┼──────────────┼──────────┤');
      for (let i = 0; i < content.length; i++) {
        const [name, open, close, current, high, low] = content[i].split(',');
        const rate = ((current - close) / close) * 100;
        console.log(`│${pad(name, 10)}│${rate > 0 ? 
          `${pad((+current).toFixed(2), 14)}`.red : `${pad((+current).toFixed(2), 14)}`.green}│${rate > 0 ? 
            `${pad(`+${rate.toFixed(2)}%`, 10)}`.red : 
            `${pad(`${rate.toFixed(2)}%`, 10)}`.green}│${pad((+close).toFixed(2), 14)}│${close - open > 0 ? 
            `${pad((+open).toFixed(2), 14)}`.green : `${pad((+open).toFixed(2), 14)}`.red}│${high - close > 0 ?
            `${pad((+high).toFixed(2), 14)}`.red : `${pad((+high).toFixed(2), 14)}`.green}│${low - close > 0 ?
              `${pad((+low).toFixed(2), 14)}`.red : `${pad((+low).toFixed(2), 14)}`.green}│${pad(`${(((high - low) / low) * 100).toFixed(2)}%`, 10)}│`
        );
        if (i === content.length - 1) {
          console.log('└──────────────┴──────────────┴──────────┴──────────────┴──────────────┴──────────────┴──────────────┴──────────┘');
        } else {
          console.log('├──────────────┼──────────────┼──────────┼──────────────┼──────────────┼──────────────┼──────────────┼──────────┤');
        }
      }
      process.exit();
    };
    http.get('http://hq.sinajs.cn/?list=sh000001,sz399417,sz399989,sz399932,sz399997,sz399241,sz399959,sz399240,sz399339', (res) => {
      const body = [];
      res.on("data", data => body.push(data));
      res.on("end", () => {
        format(iconv.decode(Buffer.concat(body), 'gbk'));
      });
    });
  });

program.parse(process.argv);